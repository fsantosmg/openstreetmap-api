package com.frwk.openstreetmap.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Localizacao {

    private String id;
    private Double latitude;
    private Double longitude;
    private String logradouro;
    private String bairro;
    private String numeroLogradouro;
    private String complemento;
    private String cep;
    private String cidade;
    private String estado;


}
