package com.frwk.openstreetmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenstreetmapApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenstreetmapApiApplication.class, args);
    }

}
