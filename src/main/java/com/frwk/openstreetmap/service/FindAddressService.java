package com.frwk.openstreetmap.service;

import com.frwk.openstreetmap.model.Localizacao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class FindAddressService {


    @Autowired
    OpenStreetMapService openStreetMapService;

    private static final Logger logger = LogManager.getLogger(FindAddressService.class);


    public Localizacao findLocalizacao(Double latitude, Double longitude) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String formatDateTime = now.format(formatter);
        Localizacao localizacao = new Localizacao();

            Long inicioGeo = System.currentTimeMillis();

            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            localizacao = openStreetMapService.find(latitude, longitude);

        return localizacao;
    }


}
