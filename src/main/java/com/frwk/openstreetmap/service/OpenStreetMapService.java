package com.frwk.openstreetmap.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.frwk.openstreetmap.model.Localizacao;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;

@Service
public class OpenStreetMapService {

    private final CloseableHttpClient httpClient = HttpClients.createDefault();
    private static final Logger logger = LogManager.getLogger(OpenStreetMapService.class);

    private RestTemplate restTemplate;


    public Localizacao find(Double latitude, Double longitude) {


        Localizacao localizacao = new Localizacao();
        localizacao.setLatitude(latitude);
        localizacao.setLongitude(longitude);

        try {

            sendGet(localizacao);
            return localizacao;

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;

    }

    public void sendGet(Localizacao localizacao) throws IOException {


        HttpGet request = new HttpGet("https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=" + localizacao.getLatitude() + "3&lon=" + localizacao.getLongitude());
        try (CloseableHttpResponse response = httpClient.execute(request)) {

            request.addHeader("service", "argosrastreamento.com.br");

            HttpEntity entity = response.getEntity();

            if (entity != null) {

                String result = EntityUtils.toString(entity, "UTF-8");
                HashMap<String, Object> resultMap = new ObjectMapper().readValue(result, HashMap.class);

                conficureLocalizacao(localizacao, (HashMap) resultMap.get("address"));

            }

        } catch (Exception e) {
            logger.error("Falha ao conectar em OpenStreetMaps", e.getMessage());

        }

    }

    private void conficureLocalizacao(Localizacao localizacao, HashMap<String, String> address) {

        localizacao.setEstado(address.get("state"));

        if (address.get("city") != null) {
            localizacao.setCidade(address.get("city"));
        } else if (address.get("town") != null) {
            localizacao.setCidade(address.get("town"));
        } else if (address.get("village") != null) {
            localizacao.setCidade(address.get("village"));
        } else if (address.get("county") != null) {
            localizacao.setCidade(address.get("county"));
        }
        localizacao.setCep(address.get("postcode"));
        localizacao.setBairro(address.get("suburb"));
        if(address.get("houseNumber")!=null){
            localizacao.setNumeroLogradouro(address.get("houseNumber"));
        }else {
            localizacao.setNumeroLogradouro(address.get("house_number"));
        }
        localizacao.setLogradouro(address.get("road"));

    }


}
