package com.frwk.openstreetmap.resource;


import com.frwk.openstreetmap.model.Localizacao;
import com.frwk.openstreetmap.service.FindAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "localization")
public class LocalizationResource {

    @Autowired
    FindAddressService findAddressService;

    @GetMapping("find")
    public ResponseEntity<Localizacao> find(@RequestParam Double latitude, @RequestParam Double longitude){

        Localizacao localizacao = findAddressService.findLocalizacao(latitude, longitude);

        return ResponseEntity.ok(localizacao);

    }

}
